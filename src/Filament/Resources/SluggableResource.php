<?php

namespace KDA\FilamentSluggable\Filament\Resources;

use KDA\FilamentSluggable\Filament\Resources\SluggableResource\Pages;
use KDA\FilamentSluggable\Filament\Resources\SluggableResource\RelationManagers;
use KDA\Sluggable\Models\Slug;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\Layout\Split;
use Filament\Tables\Columns\Layout\Stack;
use Filament\Tables\Columns\TextColumn;

class SluggableResource extends Resource
{
    protected static ?string $model = Slug::class;

    protected static ?string $navigationIcon = 'heroicon-s-tag';
    protected static ?string $navigationGroup = 'Settings';
    protected static function getNavigationGroup(): ?string
    {
        return __('filament.navigation.groups.navigation');
    }

    protected static function getNavigationLabel(): string
    {
        return __('filament-sluggable::resources.sluggable');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('slug')
                    ->required()
                    ->maxLength(255),
                Fieldset::make('Entity')
                    // ->relationship('billable')
                    ->schema([
                        Select::make('sluggable_type')
                            ->options(
                                Slug::all()->pluck('sluggable_type', 'sluggable_type')
                            )
                            ->default('App\Models\Customer')
                            ->reactive()
                            ->afterStateUpdated(function (callable $set) {
                                $set('sluggable_id', null);
                            }),
                        Select::make('sluggable_id')
                            ->searchable()
                            ->options(function (callable $get) {
                                $model = $get('sluggable_type');
                                if ($model) {
                                    return $model::all()->pluck('sluggable', 'id');
                                }
                                return [];
                            })->preload()
                            ->required()
                        /*  ->createOptionForm([
                                Forms\Components\TextInput::make('name')
                                    ->required(),

                            ])->createOptionUsing(function (array $data) {
                                return \App\Models\Customer::create($data)->getKey();
                            }),*/

                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                Split::make([
                    TextColumn::make('id')->grow(false)->toggleable()->toggledHiddenByDefault(true)->sortable(),
                    Stack::make([
                        TextColumn::make('sluggable')->formatStateUsing(fn($record)=>get_class($record->sluggable).' '.$record->sluggable?->getKey()),
                        TextColumn::make('slug')->searchable()->sortable(),
                        TextColumn::make('url')->extraAttributes(['class' => 'text-xs'])/*>url(fn (Slug $record): string => $record->url)*/->openUrlInNewTab(),
                    ])
                ])
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),

            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //

        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSluggable::route('/'),
            'create' => Pages\CreateSluggable::route('/create'),
            'edit' => Pages\EditSluggable::route('/{record}/edit'),
        ];
    }
}
