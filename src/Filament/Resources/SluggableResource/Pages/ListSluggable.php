<?php

namespace KDA\FilamentSluggable\Filament\Resources\SluggableResource\Pages;

use KDA\FilamentSluggable\Filament\Resources\SluggableResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSluggable extends ListRecords
{
    protected static string $resource = SluggableResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
