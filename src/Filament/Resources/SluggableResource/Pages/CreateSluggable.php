<?php

namespace KDA\FilamentSluggable\Filament\Resources\SluggableResource\Pages;

use KDA\FilamentSluggable\Filament\Resources\SluggableResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
class CreateSluggable extends CreateRecord
{

    protected static string $resource = SluggableResource::class;
    
}
