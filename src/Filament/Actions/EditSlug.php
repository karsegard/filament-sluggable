<?php

namespace KDA\FilamentSluggable\Filament\Actions;

use Closure;
use Filament\Facades\Filament;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Notifications\Notification;
use Filament\Tables\Actions\Action;
use Illuminate\Http\RedirectResponse;
use KDA\Sluggable\Facades\Slug;
use Livewire\Redirector;

class EditSlug extends Action
{
  
    protected function setUp(): void
    {
        parent::setUp();

        $this->modalHeading('Editer un lien');
        $this->label('Editer le lien');
        $this->icon('heroicon-o-link');

        $this->action(function (array $data): void {
            try {
                if ($data['keep_previous_link'] === false) {
                    Slug::updateSlug($this->record, $data['slug']);
                } else {
                    Slug::createSlug($this->record, $data['slug']);
                }
            } catch (\Exception $e) {
                Notification::make()
                    ->title('Error')
                    ->danger()
                    ->body($e->getMessage())
                    ->send();
            }
        });

        $this->form(function ($record) {
            if (!$record->currentSlug) {
                Slug::createSlugForModel($record);
                $record->load('currentSlug');
            }
            return [
                Toggle::make('keep_previous_link')->default(true)->reactive(),
                TextInput::make('slug')
                    ->required()
                    ->default($record->currentSlug->slug)
                    ->prefix($record->slugCollectionName() . '/')
                    ->maxLength(255)
                    ->rules([
                        function ($record, $get) {
                            $keep = $get('keep_previous_link') === false;
                            return function (string $attribute, $value, Closure $fail) use ($record, $keep) {
                                if($value === $record->currentSlug->slug){
                                    $fail('Slug is identical to current one');

                                }
                                if (Slug::getExistingSlugCount($value, $record) > 0) {
                                    $fail('Slug already exists, please choose a different one');
                                }
                            };
                        },
                    ]),
            ];
        });
    }
}
