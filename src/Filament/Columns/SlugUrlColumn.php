<?php

namespace KDA\FilamentSluggable\Filament\Columns;

use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Model;

class SlugUrlColumn extends TextColumn{
 /*   protected string $view = 'filament-sluggable::url-column';
    
    protected $copyIcon = 'heroicon-o-clipboard-copy';
    protected $urlIcon = 'heroicon-o-link';


    public function getUrlIcon():string{
        return $this->urlIcon;
    }
    

    public function getCopyIcon():string{
        return $this->copyIcon;
    }
   */ 
    public function setUp():void{
        parent::setUp();
        $this->formatStateUsing(function($record){
            return $record->url;
        });
        $this->url(fn (Model $record): string => $record->url);
        $this->openUrlInNewTab();
    }

}

