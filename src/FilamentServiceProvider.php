<?php

namespace KDA\FilamentSluggable;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
        \KDA\FilamentSluggable\Filament\Resources\SluggableResource::class,
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-sluggable');
    }
}
