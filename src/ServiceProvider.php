<?php
namespace KDA\FilamentSluggable;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasProviders;
use KDA\Laravel\Traits\HasTranslations;
use KDA\Laravel\Traits\HasViews;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasProviders;
    use HasViews;
    use HasTranslations;
    protected $packageName ="filament-sluggable";
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/filament-sluggable.php'  => 'kda.filament-sluggable'
    ];
    //-------------------------------------------
        //register filament provider
    protected $additionnalProviders=[
        \KDA\FilamentSluggable\FilamentServiceProvider::class
    ];
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
